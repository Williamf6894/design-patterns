public class Numbers {
    private int number1;
    private int number2;

    private String calculationWanted;

    public Numbers(int num1, int num2, String calcWanted) {
        number1 = num1;
        number2 = num2;
        calculationWanted = calcWanted;
    }

    public int getNumber1() {
        return number1;
    }

    public int getNumber2() {
        return number2;
    }

    public String getCalcWanted() {
        return calculationWanted;
    }
}
