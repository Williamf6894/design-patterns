public class TestCalculationChain {

    public static void main(String[] args) {
        Chain calcChain1 = new AddNumbers();
        Chain calcChain2 = new SubtractNumbers();
        Chain calcChain3 = new MultiplyNumbers();
        Chain calcChain4 = new DivideNumbers();

        calcChain1.setNextChain(calcChain2);
        calcChain2.setNextChain(calcChain3);
        calcChain3.setNextChain(calcChain4);

        Numbers request = new Numbers(4, 2, "add");
        calcChain1.calculate(request);

        request = new Numbers(5,7, "multiply");
        calcChain1.calculate(request);

        request = new Numbers(35,7, "divide");
        calcChain1.calculate(request);

        request = new Numbers(5,7, "none");
        calcChain1.calculate(request);

        request = new Numbers(4,9, "subtract");
        calcChain1.calculate(request);
    }
}
