// The chain of responsibility pattern has a
// group of objects that are expected to
// be able to solve a problem between them.

// If the first Object can't solve it, it passes
// the data to the next Object in the chain

public interface Chain {

    public void setNextChain(Chain nextChain);

    public void calculate(Numbers request);
}
