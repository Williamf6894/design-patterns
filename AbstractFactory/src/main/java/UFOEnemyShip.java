public class UFOEnemyShip extends EnemyShip {

    private EnemyShipFactory shipFactory;

    public UFOEnemyShip(EnemyShipFactory shipPartsFactory) {
        this.shipFactory = shipPartsFactory;
    }

    void makeShip() {
        System.out.print("Making enemy ship " + getName());

        weapon = shipFactory.addESGun();
        engine = shipFactory.addESEngine();
    }
}
