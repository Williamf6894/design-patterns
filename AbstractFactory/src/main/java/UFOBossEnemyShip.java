public class UFOBossEnemyShip extends EnemyShip {

    private EnemyShipFactory shipFactory;

    public UFOBossEnemyShip(EnemyShipFactory shipPartsFactory) {
        this.shipFactory = shipPartsFactory;
    }

    void makeShip() {
        System.out.print("Making enemy ship" + getName());

        weapon = shipFactory.addESGun();
        engine = shipFactory.addESEngine();
    }

}
