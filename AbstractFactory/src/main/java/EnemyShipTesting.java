public class EnemyShipTesting {

    public static void main(String[] args) {
        EnemyShipBuilding ufoMaker;
        ufoMaker = new UFOEnemyShipBuilding();

        EnemyShip theGrunt = ufoMaker.orderTheShip("UFO");
        System.out.println(theGrunt + "\n");

        EnemyShip theBoss = ufoMaker.orderTheShip("UFO BOSS");
        System.out.println(theBoss + "\n");
    }
}
