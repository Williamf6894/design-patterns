import java.util.ArrayList;
import java.util.List;

public class PlayWithRemote {

    public static void main(String[] args) {
        ElectronicDevice newDevice = TVRemote.getDevice();

        TurnTVOnCommand onCommand = new TurnTVOnCommand(newDevice);
        DeviceButton onPressed = new DeviceButton(onCommand);
        onPressed.press();

        TurnTVOffCommand offCommand = new TurnTVOffCommand(newDevice);
        onPressed = new DeviceButton(offCommand);
        onPressed.press();

        TurnVolumeUpCommand volumeUpCommand = new TurnVolumeUpCommand(newDevice);
        onPressed = new DeviceButton(volumeUpCommand);
        onPressed.press();
        onPressed.press();
        onPressed.press();
        onPressed.pressUndo();

        Television theTV = new Television();
        Radio theRadio = new Radio();

        List<ElectronicDevice> allDevices = new ArrayList<ElectronicDevice>();

        allDevices.add(theTV);
        allDevices.add(theRadio);

        TurnEverythingOff devicesToRestart = new TurnEverythingOff(allDevices);
        DeviceButton shutdown = new DeviceButton(devicesToRestart);
        shutdown.press();

        shutdown.pressUndo();

    }

}
