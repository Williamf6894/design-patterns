public class TurnTVOnCommand implements Command {

    ElectronicDevice theDevice;

    public TurnTVOnCommand(ElectronicDevice device) {
        theDevice = device;
    }

    public void execute() {
        theDevice.on();
    }

    public void undo() {
        theDevice.off();
    }
}
