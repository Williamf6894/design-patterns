public class TurnTVOffCommand implements Command {

    private ElectronicDevice theDevice;

    public TurnTVOffCommand(ElectronicDevice device) {
        theDevice = device;
    }

    public void execute() {
        theDevice.off();
    }

    public void undo() {
        theDevice.on();
    }
}
