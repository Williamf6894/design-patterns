public class TurnVolumeUpCommand implements Command{

    private ElectronicDevice theDevice;

    public TurnVolumeUpCommand(ElectronicDevice device) {
        theDevice = device;
    }

    public void execute() {
        theDevice.volumeUp();
    }

    public void undo() {
        theDevice.volumeDown();
    }
}
