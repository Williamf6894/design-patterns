import java.util.List;

public class TurnEverythingOff implements Command {

    List<ElectronicDevice> theDevices;

    public TurnEverythingOff(List<ElectronicDevice> newDevice) {
        theDevices = newDevice;
    }

    public void execute() {
        for (ElectronicDevice device : theDevices) {
            device.off();
        }
    }

    public void undo() {
        for (ElectronicDevice device : theDevices) {
            device.on();
        }
    }
}
