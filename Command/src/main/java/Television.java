
// Television is the receiver of the commands
public class Television implements ElectronicDevice {

    private int volume = 0;

    public void on() {
        System.out.println("TV is on");
    }

    public void off() {
        System.out.println("TV is off");
    }

    public void volumeUp() {
        System.out.println("TV volume is at: " + ++volume);
    }

    public void volumeDown() {
        System.out.println("TV volume is at: " + --volume);
    }
}
