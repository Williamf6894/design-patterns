
// Radio is also a receiver like Television
public class Radio implements ElectronicDevice{
    private int volume = 0;


    public void on() {
        System.out.println("Radio is on");
    }

    public void off() {
        System.out.println("Radio is off");
    }

    public void volumeUp() {
        System.out.println("Radio volume is at: " + ++volume);
    }

    public void volumeDown() {
        System.out.println("Radio volume is at: " + --volume);
    }
}
