public class Originator {

    private String article;

    public void set(String newArticle) {
        print("From Originator: Current version of Article\n" + newArticle);
        this.article = newArticle;
    }

    public Memento storeInMemento() {
        print("From Originator: Saving to Memento");
        return new Memento(article);
    }

    public String restoreFromMemento(Memento memento) {
        article = memento.getSavedArticle();
        print("From Originator: Previous Article Saved in Memento\n " + article);
        return article;
    }

    private void print(String out) {
        System.out.println(out);
    }
}
