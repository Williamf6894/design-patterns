
// Stores an objects state at a point in time
// so it can be return to that state later. It
// simple allow you to undo/redo change on an object

public class Memento {

    private String article;

    public Memento(String articleSave) {
        article = articleSave;
    }

    public String getSavedArticle() {
        return article;
    }

}
