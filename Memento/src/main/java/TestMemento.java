import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TestMemento extends JFrame {

    public static void main(String[] args) {
        new TestMemento();
    }

    private JButton saveButton, undoButton, redoButton;
    private JTextArea theArticle = new JTextArea(40,60);

    Caretaker caretaker = new Caretaker();

    Originator originator = new Originator();

    int saveFiles = 0, currentArticle = 0;

    public TestMemento() {
        this.setSize(750,750);
        this.setTitle("Memento Design Pattern");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel1 = new JPanel();
        panel1.add(new Label("Article"));
        panel1.add(theArticle);

        ButtonListener saveListener = new ButtonListener();
        ButtonListener undoListener = new ButtonListener();
        ButtonListener redoListener = new ButtonListener();

        saveButton = new JButton("Save");
        saveButton.addActionListener((ActionListener) saveListener);
        undoButton = new JButton("Undo");
        undoButton.addActionListener((ActionListener) undoListener);
        redoButton = new JButton("Redo");
        redoButton.addActionListener((ActionListener) redoListener);

        panel1.add(saveButton);
        panel1.add(undoButton);
        panel1.add(redoButton);

        this.add(panel1);
        this.setVisible(true);

    }


    private class ButtonListener implements ActionListener {

        public void actionPerformed(ActionEvent actionEvent) {
            if (actionEvent.getSource() == saveButton) {
                saveTextToMemento(theArticle.getText());
                undoButton.setEnabled(true);
            }

            else if (actionEvent.getSource() == undoButton) {
                if (canUndo()) {
                    redoButton.setEnabled(true);
                    undoChangesToArticle();
                } else {
                    undoButton.setEnabled(false);
                }

            } else if (actionEvent.getSource() == redoButton) {
                if ((saveFiles - 1) > currentArticle) {
                    redoChangesToArticle();
                    undoButton.setEnabled(true);
                } else {
                    redoButton.setEnabled(false);
                }
            }
        }

        private void saveTextToMemento(String text) {
            originator.set(text);
            caretaker.addMemento(originator.storeInMemento());
            saveFiles++;
            currentArticle++;
        }

        private boolean canUndo(){
            return currentArticle >= 1;
        }

        private void undoChangesToArticle() {
            currentArticle--;
            String textBoxString = getLastChange();
            theArticle.setText(textBoxString);
        }

        private String getLastChange() {
            return originator.restoreFromMemento(caretaker.getMemento(currentArticle));
        }

        private void redoChangesToArticle() {
            currentArticle++;
            String textBoxString = originator.restoreFromMemento(caretaker.getMemento(currentArticle));
            theArticle.setText(textBoxString);
        }

    }
}
