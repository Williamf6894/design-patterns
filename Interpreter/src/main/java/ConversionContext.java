public class ConversionContext {

    private String conversionQuestion = "";
    private String conversionResponse = "";
    private String fromConversion = "";
    private String toConversion = "";
    private double quantity;

    String[] partsOfQuestion;

    public ConversionContext(String questionAsked) {
        this.conversionQuestion = questionAsked;

        partsOfQuestion = getInput().split(" ");
        fromConversion = getCapitalized(partsOfQuestion[1]);
        toConversion = getLowerCase(partsOfQuestion[3]);
        quantity = Double.valueOf(partsOfQuestion[0]);

        conversionResponse = partsOfQuestion[0] + " " + partsOfQuestion[1] + " equals ";
    }

    public String getLowerCase(String wordToLowercase) {
        return wordToLowercase.toLowerCase();
    }

    public String getCapitalized(String wordToCapitalize) {

        wordToCapitalize = wordToCapitalize.toLowerCase();

        // Returns the Uppercase of the first char and then rest of the string following it.
        wordToCapitalize = Character.toUpperCase(wordToCapitalize.charAt(0)) + wordToCapitalize.substring(1);

        int lengthOfWord = wordToCapitalize.length();
        if(( wordToCapitalize.charAt(lengthOfWord - 1) != 's' )) {
            wordToCapitalize = new StringBuffer(wordToCapitalize).insert(lengthOfWord, "s").toString();
        }

        return wordToCapitalize;
    }

    public String getInput() {
        return this.conversionQuestion;
    }

    public String getFromConversion() {
        return this.fromConversion;
    }

    public String getToConversion() {
        return  this.toConversion;
    }

    public String getConversionResponse() {
        return this.conversionResponse;
    }

    public double getQuantity() {
        return this.quantity;
    }
}
