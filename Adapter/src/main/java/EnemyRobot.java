import java.util.Random;

// This is the Adaptee. The Adapter sends the method calls to objects that user the EnemyAttack interface.

public class EnemyRobot {

    Random randGen = new Random();
    public void smashWithHands() {
        int attackDamage = randGen.nextInt(10) + 1;
        System.out.println("Enemy Robot causes " + attackDamage + " damage with its hands.");
    }

    public void walkForward() {
        int movement = randGen.nextInt(5) + 1;
        System.out.println("Enemy Robot walks forward " + movement + " spaces");
    }

    public void reactToHuman(String driverName) {
        System.out.println("Enemy Robot Tramps on " + driverName);
    }

}
