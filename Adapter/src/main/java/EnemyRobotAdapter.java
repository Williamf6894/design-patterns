public class EnemyRobotAdapter implements EnemyAttacker{

    EnemyRobot enemyRobot;

    public EnemyRobotAdapter(EnemyRobot newRobot) {
        enemyRobot = newRobot;
    }

    public void fireWeapon() {
        enemyRobot.smashWithHands();
    }

    public void driveForward() {
        enemyRobot.walkForward();
    }

    public void assignDriver(String driverName) {
        enemyRobot.reactToHuman(driverName);
    }
}
