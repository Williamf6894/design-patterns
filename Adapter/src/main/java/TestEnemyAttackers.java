public class TestEnemyAttackers {

    public static void main(String[] args) {

        EnemyTank enemyTank = new EnemyTank();
		EnemyRobot fredTheRobot = new EnemyRobot();

		EnemyAttacker robotAdapter = new EnemyRobotAdapter(fredTheRobot);

		System.out.println("The Robot");
		fredTheRobot.reactToHuman("Paul");
		fredTheRobot.walkForward();
		fredTheRobot.smashWithHands();
		System.out.println();

		System.out.println("The Enemy Tank");
		enemyTank.assignDriver("Frank");
		enemyTank.driveForward();
		enemyTank.fireWeapon();
		System.out.println();

		System.out.println("The Robot with Adapter");
		robotAdapter.assignDriver("Mark");
		robotAdapter.driveForward();
		robotAdapter.fireWeapon();

    }
}
