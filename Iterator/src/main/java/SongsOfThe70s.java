import java.util.ArrayList;
import java.util.Iterator;

public class SongsOfThe70s implements SongIterator{
    ArrayList<SongInformation> bestSongs;

    public SongsOfThe70s() {
        bestSongs = new ArrayList<SongInformation>();
        addSong("Imagine", "John Lennon", 1971);
        addSong("American Pie", "Don McLean", 1971);
        addSong("I Will Survive", "Gloria Gaynor", 1979);
    }

    public void addSong(String songName, String bandName, int yearReleased) {
        SongInformation songInfo = new SongInformation(songName, bandName, yearReleased);
        bestSongs.add(songInfo);
    }

    public ArrayList<SongInformation> getBestSongs() {
        return bestSongs;
    }

    public Iterator createIterator() {
        return bestSongs.iterator();
    }

}
