public class RadioStation {

    public static void main(String[] args) {

        // Check each one, they are unique implementations
        SongsOfThe70s songs70s = new SongsOfThe70s();
        SongsOfThe80s songs80s = new SongsOfThe80s();
        SongsOfThe90s songs90s = new SongsOfThe90s();

        DiskJockey yourManFromDrivetime = new DiskJockey(songs70s, songs80s, songs90s);

        yourManFromDrivetime.showTheSongs();
    }
}
