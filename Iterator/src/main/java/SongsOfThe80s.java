import java.util.Arrays;
import java.util.Iterator;

public class SongsOfThe80s implements SongIterator{

    SongInformation[] bestSongs;
    int arrayValue = 0;

    public SongsOfThe80s() {
		bestSongs = new SongInformation[3];

		addSong("Roam", "B 52s", 1989);
		addSong("Cruel Summer", "Bananarama", 1984);
		addSong("Head Over Heels", "Tears For Fears", 1985);
	}

    public void addSong(String songName, String bandName, int yearReleased){
		SongInformation song = new SongInformation(songName, bandName, yearReleased);

		bestSongs[arrayValue] = song;
		arrayValue++;
	}

    public SongInformation[] getBestSongs(){
		return bestSongs;
	}

    // By adding this method I'll be able to treat all
	// collections the same
	public Iterator createIterator() {
		return Arrays.asList(bestSongs).iterator();
	}
}
