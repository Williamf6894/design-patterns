import java.util.Hashtable;
import java.util.Iterator;

public class SongsOfThe90s implements SongIterator{

	Hashtable<Integer, SongInformation> bestSongs = new Hashtable<Integer, SongInformation>();
    int hashKey = 0;

    public SongsOfThe90s() {

		addSong("Losing My Religion", "REM", 1991);
		addSong("Creep", "Radiohead", 1993);
		addSong("Walk on the Ocean", "Toad The Wet Sprocket", 1991);
	}

    public void addSong(String songName, String bandName, int yearReleased){

		SongInformation songInfo = new SongInformation(songName, bandName, yearReleased);
		bestSongs.put(hashKey, songInfo);
		hashKey++;
	}

    public Hashtable<Integer, SongInformation> getBestSongs(){
		return bestSongs;
	}

	public Iterator createIterator() {
		return bestSongs.values().iterator();
	}
}
