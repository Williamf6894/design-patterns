import java.util.ArrayList;
import java.util.Iterator;

public class DiskJockey {

    SongIterator songs70sIter;
    SongIterator songs80sIter;
    SongIterator songs90sIter;

    public DiskJockey(SongIterator new70s, SongIterator new80s, SongIterator new90s) {
        songs70sIter = new70s;
        songs80sIter = new80s;
        songs90sIter = new90s;
    }

    // This iterator allows us to have unique implementations and still access the data in the same way
    // Otherwise we would need a different handling method for each collection.
    public void showTheSongs() {
        System.out.println("Using Iterators");

        Iterator Songs70s = songs70sIter.createIterator();
        Iterator Songs80s = songs80sIter.createIterator();
        Iterator Songs90s = songs90sIter.createIterator();

        System.out.println("Songs of the 70s\n");
        printTheSongs(Songs70s);
        System.out.println("Songs of the 80s\n");
        printTheSongs(Songs80s);
        System.out.println("Songs of the 90s\n");
        printTheSongs(Songs90s);

    }

    private void printTheSongs(Iterator iterator) {
        while(iterator.hasNext()) {
            SongInformation songinfo = (SongInformation) iterator.next();

            System.out.println(songinfo.getSongName());
            System.out.println(songinfo.getBandName());
            System.out.println(songinfo.getYearReleased());
            System.out.println();
        }
    }
}
