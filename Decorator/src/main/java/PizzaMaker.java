public class PizzaMaker {
    public static void main(String[] args) {

        // The Decorator and PlainPizza implement Pizza
        // All Toppings are extended from Decorator, there implement Pizza.

        Pizza basicPizza = new TomatoSauceTopping(new MozzarellaTopping(new PlainPizza()));
        System.out.println("Ingredients: " + basicPizza.getDescription());
        System.out.println("Price: " + basicPizza.getCost());

    }
}
