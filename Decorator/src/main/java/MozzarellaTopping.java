public class MozzarellaTopping extends ToppingDecorator {

    public MozzarellaTopping(Pizza pizza) {
        super(pizza);
        System.out.println("Adding Dough");
        System.out.println("Adding Moz");
    }

    public String getDescription() {
        return tempPizza.getDescription() + ", mozzarella";
    }

    public double getCost() {
        System.out.println("Cost of Moz: " + .50);
        return tempPizza.getCost() + .50;
    }
}
