public class TomatoSauceTopping extends ToppingDecorator {

	public TomatoSauceTopping(Pizza newPizza) {
		super(newPizza);
		System.out.println("Adding Sauce");
	}
	// Returns the result of calling getDescription() for
	// PlainPizza, MozzarellaTopping and then TomatoSauce

	public String getDescription(){
		return tempPizza.getDescription() + ", tomato sauce";
	}

	public double getCost(){
		System.out.println("Cost of Sauce: " + .35);
		return tempPizza.getCost() + .35;
	}

}
