public class TestBankAccount {

    public static void main(String[] args) {

        BankAccountFacade accessingBank = new BankAccountFacade(12345678, 1234);

        accessingBank.withdrawCash(50.00); // good amount
        accessingBank.withdrawCash(990.00); // Too much
        accessingBank.depositCash(150.00);
    }
}
