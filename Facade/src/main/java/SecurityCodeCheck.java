public class SecurityCodeCheck {

    private int secNumber = 1234;

    public int getSecurityNumber() {
        return secNumber;
    }

    public boolean isCodeCorrect(int secNumberToCheck) {
        return (secNumberToCheck == getSecurityNumber());
    }
}
