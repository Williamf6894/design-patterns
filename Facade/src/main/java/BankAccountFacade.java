
// The Facade Desing Pattern decouples or separates the client from all of the subs components.
// The Facades aim is to simplify interfaces so you don't have to worry about what is going on under the hood
public class BankAccountFacade {

    private int accountNumber;
    private int securityNumber;

    AccountNumberCheck accountNumberCheck;
    SecurityCodeCheck securityCodeCheck;
    FundsCheck fundsCheck;

    WelcomeToBank welcome;

    public BankAccountFacade(int accountNumber, int secNumber) {
        this.accountNumber = accountNumber;
        this.securityNumber = secNumber;

        welcome = new WelcomeToBank();
        accountNumberCheck = new AccountNumberCheck();
        securityCodeCheck = new SecurityCodeCheck();
        fundsCheck = new FundsCheck();

    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public int getSecurityNumber() {
        return securityNumber;
    }

    public void withdrawCash(double cashRequested) {
        if ( accountNumberCheck.accountActive(getAccountNumber()) &&
            securityCodeCheck.isCodeCorrect(getSecurityNumber()) &&
            fundsCheck.haveEnoughMoney(cashRequested) )
        {

            System.out.println("Transaction Complete!\n");
        } else {
            System.out.println("Transaction Failed\n");
        }
    }

    public void depositCash(double cashToDeposit) {
        if ( accountNumberCheck.accountActive(getAccountNumber()) &&
            securityCodeCheck.isCodeCorrect(getSecurityNumber()) )
        {

            fundsCheck.makeDeposit(cashToDeposit);
            System.out.println("Transaction Complete!\n");
        } else {
            System.out.println("Transaction Failed\n");
        }

    }
}
