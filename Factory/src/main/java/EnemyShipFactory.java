public class EnemyShipFactory {

    public EnemyShip makeEnemyShip(String shipType) {
        EnemyShip newShip = null;

        if (shipType.equalsIgnoreCase("U")) {
            return new UFOEnemyShip();
        }
        if (shipType.equalsIgnoreCase("R")) {
            return new RocketEnemyShip();
        }
        if (shipType.equalsIgnoreCase("B")) {
            return new BigUFOEnemyShip();
        }

        return null;
    }
}
