import java.util.Scanner;

public class EnemyShipTests {

    public static void main(String[] args) {

        EnemyShipFactory shipFactory = new EnemyShipFactory();
        EnemyShip theEnemy = null;

        theEnemy = shipFactory.makeEnemyShip(getUserInput());

        if (theEnemy != null) {
            doEnemyThings(theEnemy);
        }
        else {
            System.out.println("Please enter U, R or B next time.");
        }

    }

    private static String getUserInput() {
        System.out.println("Enter U, R or B please.");
        // Assume good user, no need to validate info
        Scanner userInput = new Scanner(System.in);
        String typeOfShip = "";

        if(userInput.hasNextLine()) {
            typeOfShip = userInput.nextLine();
            userInput.close();
        }

        return typeOfShip;
    }

    private static void doEnemyThings(EnemyShip enemy) {
        enemy.displayEnemyShip();
        enemy.followHeroShip();
        enemy.enemyShipShoots();
    }
}
