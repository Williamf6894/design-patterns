public abstract class SubSandwich {

    boolean afterFirstCondiment = false;


    // This is the template method
    // Declare this method as final to keep subclasses from changing the algorithm
    final void makeSandwich() {

        cutBun();

        if (customerWantsMeat()) {
            addMeat();
            afterFirstCondiment = true;
        }

        if (customerWantsCheese()) {
            afterFirstCondiment();
            addCheese();
            afterFirstCondiment = true;
        }

        if (customerWantsVegetables()) {
            afterFirstCondiment();
            addVegetables();
            afterFirstCondiment = true;
        }

        if (customerWantsCondiments()) {
            afterFirstCondiment();
            addCondiments();
            afterFirstCondiment = true;
        }

        wrapTheSub();

    }

    public void cutBun() {
        System.out.println("Sub is cut");
    }

    abstract void addMeat();
    abstract void addCheese();
    abstract void addVegetables();
    abstract void addCondiments();

    boolean customerWantsMeat() {
        return true;
    }

    boolean customerWantsCondiments() {
        return true;
    }

    boolean customerWantsVegetables() {
        return true;
    }

    boolean customerWantsCheese() {
        return true;
    }


    protected void wrapTheSub() {
        System.out.println("Wrapped the sandwich");
    }

    private void afterFirstCondiment() {
        if (afterFirstCondiment) {
            System.out.print('\n');
        }
    }




}
