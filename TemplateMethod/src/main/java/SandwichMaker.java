public class SandwichMaker {

    public static void main(String[] args) {

        ItalianSub cust1Sub = new ItalianSub();
        cust1Sub.makeSandwich();

        System.out.println();

        VeggieSub cust2Sub = new VeggieSub();
        cust2Sub.makeSandwich();

    }
}
