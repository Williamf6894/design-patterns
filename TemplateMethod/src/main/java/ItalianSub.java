public class ItalianSub extends SubSandwich {

    String[] meatUsed = { "Salami", "Pepperoni", "Ham" };
    String[] cheeseUsed = { "Provolone" };
    String[] veggieUsed = { "Lettuce", "Tomatoes", "Onion", "Sweet Pepper"};
    String[] condimentsUsed = { "Oil", "Vinegar" };

    public void addMeat() {
        System.out.println("Adding the meat: ");
        addIngredients(meatUsed);
    }
    public void addCheese() {
        System.out.println("Adding the cheese: ");
        addIngredients(cheeseUsed);
    }
    public void addVegetables() {
        System.out.println("Adding the meat: ");
        addIngredients(veggieUsed);
    }
    public void addCondiments() {
        System.out.println("Adding the condiments: ");
        addIngredients(condimentsUsed);
    }

    protected void wrapTheSub() {

    }

    private void addIngredients(String[] items) {
        for (String item : items) {
            System.out.print(item + " ");
        }
    }
}
