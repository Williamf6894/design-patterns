// Called when the subject changes
public interface Observer {

    public void update(double ibmPrice, double aaplPrice, double googPrice);
}
