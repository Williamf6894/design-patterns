public class StockObserver implements Observer {

    private double ibmPrice;
    private double aaplPrice;
    private double googPrice;

    private static int observerIDTracker = 0;

    private int observerID;

    private Observable stockGrabber;

    public StockObserver(Observable stockGrabber) {
        this.stockGrabber = stockGrabber;

        setID();
        System.out.println("New Observer " + this.observerID);
        stockGrabber.subscribe(this);
    }

    private void setID() {
        this.observerID = ++observerIDTracker;
    }

    public void update(double ibmPrice, double aaplPrice, double googPrice) {
        this.ibmPrice = ibmPrice;
        this.aaplPrice = aaplPrice;
        this.googPrice = googPrice;

        printAllPrices();
    }

    private void printAllPrices() {
        System.out.println(observerID + "\nIBM: " + ibmPrice + "\nAAPL: " +
                aaplPrice + "\nGOOG: " + googPrice + "\n");
    }
}
