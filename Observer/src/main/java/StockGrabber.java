import java.util.ArrayList;

public class StockGrabber implements Observable {
    private ArrayList<Observer> allObservers;
    private double ibmPrice;
    private double aaplPrice;
    private double googPrice;

    public StockGrabber() {
        allObservers = new ArrayList<Observer>();
    }

    public void subscribe(Observer observer) {
        allObservers.add(observer);
    }

    public void unsubscribe(Observer observerToRemove) {

        int observerIndex = allObservers.indexOf(observerToRemove);
        allObservers.remove(observerIndex);

//        Can also be done like this
//        allObservers.remove(observerToRemove);
    }

    public void notifyObserver() {
        for(Observer observer : allObservers){
            observer.update(ibmPrice, aaplPrice, googPrice);
        }
    }

    public void setAAPLPrice(double aaplPrice) {
        this.aaplPrice = aaplPrice;
        notifyObserver();
    }

    public void setGOOGPrice(double googPrice) {
        this.googPrice = googPrice;
        notifyObserver();
    }

    public void setIBMPrice(double ibmPrice) {
        this.ibmPrice = ibmPrice;
        notifyObserver();
    }
}
