import java.text.DecimalFormat;

public class GetAStock implements Runnable {
    private String stock;
    private double price;
    private int updateRateInSeconds;
    DecimalFormat df = new DecimalFormat("#.##");

    private Observable stockGrabber;

    public GetAStock(Observable stockGrabber, int updateRate, String stockName, double price) {
        this.stockGrabber = stockGrabber;
        this.stock = stockName;
        this.updateRateInSeconds = updateRate;
        this.price = price;
    }

    public void run(){
        for(int i = 1; i <= 20; i++){

			try{
				Thread.sleep(this.updateRateInSeconds * 1000);
			}
			catch(InterruptedException e) { }

            double priceVariation = randomStockVariation();
	        price = Double.valueOf(df.format(price + priceVariation));

	        updateStockPrices();
	        printStockInformation(price, priceVariation);
		}
    }

    private double randomStockVariation() {
    	return (Math.random() * (.06)) - .03;
	}

	private void updateStockPrices() {
        if(stock == "IBM") ((StockGrabber) stockGrabber).setIBMPrice(price);
        if(stock == "AAPL") ((StockGrabber) stockGrabber).setAAPLPrice(price);
        if(stock == "GOOG") ((StockGrabber) stockGrabber).setGOOGPrice(price);
    }

    private void printStockInformation(double price, double priceVariation){
        System.out.println(stock + ": " + df.format((price + priceVariation)) +
                " " + df.format(priceVariation));
        System.out.println();
    }

}
