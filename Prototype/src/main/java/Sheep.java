public class Sheep implements Animal{

    public Sheep() {
        System.out.println("Sheep was made");
    }

    public Animal makeCopy(){
        System.out.println("Sheep was made");

        Sheep sheep = null;
        try {
            sheep = (Sheep) super.clone();
        }
        catch(CloneNotSupportedException e) {
            System.out.println("The Sheep was... not a sheep.");
            e.printStackTrace();
        }

        return sheep;
    }

    public String toString() {
        return "Dolly is lives! Baaa";
    }

}
