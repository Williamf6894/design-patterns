public class CloneFactory {

    // Returns a clone of any animal or animal subclass

    // CloneFactory has no idea what these object are except that they are subclasses of animal
    Animal getClone(Animal animalSample){
        return animalSample.makeCopy();
    }
}
