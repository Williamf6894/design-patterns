public class TestCloning {

    public static void main(String[] args){

        CloneFactory animalMaker = new CloneFactory();

        Sheep dolly = new Sheep();
        Sheep clonedDolly = (Sheep) animalMaker.getClone(dolly);

        System.out.println(dolly);
		System.out.println(clonedDolly);
		System.out.println("Dolly HashCode: " + System.identityHashCode( System.identityHashCode(dolly) ));
		System.out.println("Clone HashCode: " + System.identityHashCode( System.identityHashCode(clonedDolly) ));
    }
}
