public class AbstractDataBridge implements IAbstractDataBridge{

    public IBridge bridge;

    public AbstractDataBridge( IBridge bridge ) {
        this.bridge = bridge;
    }

    public void storeData() {
        this.bridge.saveData();
    }

    public void retrieveData() {
        this.bridge.loadData();
    }
}
