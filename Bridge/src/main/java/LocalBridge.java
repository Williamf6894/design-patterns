public class LocalBridge implements IBridge {

    public void saveData() {
        System.out.println("Saving to Local");
    }

    public void loadData() {
        System.out.println("Loading from Local");
    }
}
