public class DatabaseBridge implements IBridge {

    public void saveData() {
        System.out.println("Saving to DB");
    }

    public void loadData() {
        System.out.println("Loading from DB");
    }
}
