public interface IBridge {
    void saveData();
    void loadData();
}
