public interface IAbstractDataBridge {
    void storeData();
    void retrieveData();
}
