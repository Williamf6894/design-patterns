public class BridgeDemo {

    public static void main(String[] args) {
        IBridge localStoreBridge = new LocalBridge();
        IBridge databaseBridge = new DatabaseBridge();


        IAbstractDataBridge dataController;
        dataController = new AbstractDataBridge(localStoreBridge);
        dataController.storeData();
        dataController.retrieveData();

        dataController = new AbstractDataBridge(databaseBridge);
        dataController.storeData();
        dataController.retrieveData();
    }
}
