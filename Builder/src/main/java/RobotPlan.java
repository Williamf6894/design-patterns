
// This is the interface that will be returned from the builder
public interface RobotPlan {
    public void setRobotHead(String head);
    void setRobotCore(String core);
    void setRobotArms(String arms);
    void setRobotLegs(String legs);
}
