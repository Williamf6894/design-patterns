public class TestRobotBuilder {

    public static void main(String[] args) {

        RobotBuilder oldRobot = new RobotBuilderImpl();

        RobotEngineer robotEngineer = new RobotEngineer(oldRobot);
        robotEngineer.makeRobot();

        Robot firstRobot = robotEngineer.getRobot();

        System.out.println("Robot is made");
        System.out.println("Robot head type: " + firstRobot.getRobotHead());
        System.out.println("Robot body type: " + firstRobot.getRobotCore());
        System.out.println("Robot arms type: " + firstRobot.getRobotArms());
        System.out.println("Robot legs type: " + firstRobot.getRobotLegs());
    }

}
