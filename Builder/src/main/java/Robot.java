
// The concrete Robot class based on the RobotPlan interface
public class Robot implements RobotPlan {
    private String robotHead;
    private String robotCore;
    private String robotArms;
    private String robotLegs;

    public String getRobotHead() {
        return robotHead;
    }

    public void setRobotHead(String robotHead) {
        this.robotHead = robotHead;
    }

    public String getRobotCore() {
        return robotCore;
    }

    public void setRobotCore(String robotCore) {
        this.robotCore = robotCore;
    }

    public String getRobotArms() {
        return robotArms;
    }

    public void setRobotArms(String robotArms) {
        this.robotArms = robotArms;
    }

    public String getRobotLegs() {
        return robotLegs;
    }

    public void setRobotLegs(String robotLegs) {
        this.robotLegs = robotLegs;
    }
}
