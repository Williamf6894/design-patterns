
// The concrete builder class that assembles the parts
public class RobotBuilderImpl implements RobotBuilder {

    private Robot robot;

    public RobotBuilderImpl() {
        this.robot = new Robot();
    }

    public void buildRobotHead() {
        robot.setRobotHead("Tin head");
    }

    public void buildRobotCore() {
        robot.setRobotCore("Steel Body");
    }

    public void buildRobotArms() {
        robot.setRobotArms("Blowtorch arms");
    }

    public void buildRobotLegs() {
        robot.setRobotLegs("Roller Skates");
    }

    public Robot getRobot() {
        return this.robot;
    }
}
