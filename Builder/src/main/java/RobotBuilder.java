public interface RobotBuilder {

    public void buildRobotHead();
    public void buildRobotCore();
    public void buildRobotArms();
    public void buildRobotLegs();

    public Robot getRobot();
}
