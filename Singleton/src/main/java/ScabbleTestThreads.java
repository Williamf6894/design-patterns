public class ScabbleTestThreads {

    public static void main(String[] args) {

        // Create a new thread with the Runnable Interface

        Runnable getTiles = new GetTheTiles();
        Runnable getTilesAgain = new GetTheTiles();

        new Thread(getTiles).start();
        new Thread(getTilesAgain).start();
    }
}
