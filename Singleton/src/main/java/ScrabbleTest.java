import java.util.LinkedList;

public class ScrabbleTest {

    public static void main(String[] args) {

        Singleton firstInstance = Singleton.getInstance();

        System.out.println("1st Instance ID: " + System.identityHashCode( firstInstance ));
        LinkedList<String> playerOneTiles = firstInstance.getTiles(7);
        System.out.println("Player tiles: " + playerOneTiles);
        System.out.println(firstInstance.getLetterList());

        // Second Call somewhere else in the system
        Singleton secondInstance = Singleton.getInstance();

        System.out.println("2nd Instance ID: " + System.identityHashCode( secondInstance ));
        LinkedList<String> playerTwoTiles = secondInstance.getTiles(7);
        System.out.println("Player tiles: " + playerTwoTiles);
        System.out.println(secondInstance.getLetterList());
    }


}
