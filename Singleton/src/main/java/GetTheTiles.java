import java.util.LinkedList;

public class GetTheTiles implements Runnable {
    
    public void run() {
        
        Singleton newInstance = Singleton.getInstance();
        
        System.out.println("1st Instance ID: " + System.identityHashCode( newInstance ));
        LinkedList<String> playerOneTiles = newInstance.getTiles(7);
        System.out.println("Player 1 tiles: " + playerOneTiles);
        System.out.println(newInstance.getLetterList());

        System.out.println("Got the Tiles");
    }
}
