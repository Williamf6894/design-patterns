public class ATMProxy implements GetATMData{

    public ATMState getATMState() {
        ATMMachine realATM = new ATMMachine();
        return realATM.getATMState();
    }

    public int getCashInMachine() {
        ATMMachine realATM = new ATMMachine();
        return realATM.getCashInMachine();
    }
}
