public class ATMMachine implements GetATMData, ATMState{

    private int cashInMachine = 3000;
    private ATMState atmState = null;

    private ATMState hasCard;
    private ATMState noCard;
    private ATMState hasCorrectPin;
    private ATMState atmOutOfMoney;


    public ATMState getYesCardState() { return hasCard; }
    public ATMState getNoCardState() { return noCard; }
    public ATMState getHasPin() { return hasCorrectPin; }
    public ATMState getNoCashState() { return atmOutOfMoney; }

    // New stuff from the GetATMData interface
    // This is for the proxy

    public ATMState getATMState() {
        return atmState;
    }

    public int getCashInMachine() {
        return cashInMachine;
    }

    public void insertCard() {
    }

    public void ejectCard() {
    }
    
    public void insertPin(int pinNumber) {
        // Check if its the right pin
    }

    public void requestCash(int cashToWithdraw) {
        // Check if the cash is there
    }

}
