public class TestATMMachine {

    // Just so its clear, I know that ATM stands for Auto Telling Machine and that the class is poorly named.
    // It just sounds better. Even if its wrong.

    public static void main(String[] args) {

        ATMMachine atmMachine = new ATMMachine();

        atmMachine.insertCard();
        atmMachine.ejectCard();
        atmMachine.insertCard();
        atmMachine.insertPin(6894);
        atmMachine.requestCash(2000);
        atmMachine.insertCard();
        atmMachine.insertPin(6894);

        // atmMachine has full access to all of the methods. But the proxy is limited to just the GetATMData interface

        GetATMData realATMMachine = new ATMMachine();
        GetATMData proxyATMMachine = new ATMProxy();

        System.out.println("\nCurrent ATM State " + proxyATMMachine.getATMState());
        System.out.println("\nCash in ATM Machine $" + proxyATMMachine.getCashInMachine());

        // The Proxy cannot run this line though. It has no access to that interface.
        // atmProxy.setCashInMachine(10000);

    }
}
