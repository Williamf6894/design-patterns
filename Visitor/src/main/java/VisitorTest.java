public class VisitorTest {

    public static void main(String[] args) {

        TaxVisitor taxCalc = new TaxVisitor();
        TaxHolidayVisitor taxHolidayCalc = new TaxHolidayVisitor();

        Necessity milk = new Necessity(2.60);
        Liquor vodka = new Liquor(12.99);
        Tobacco cigarettes = new Tobacco(5.55);

        System.out.println("\nSTANDARD PRICES");
        System.out.println(milk.accept(taxCalc));
        System.out.println(vodka.accept(taxCalc));
        System.out.println(cigarettes.accept(taxCalc));

        System.out.println("\nHOLIDAY PRICES");
        System.out.println(milk.accept(taxHolidayCalc));
        System.out.println(vodka.accept(taxHolidayCalc));
        System.out.println(cigarettes.accept(taxHolidayCalc));
    }
}
