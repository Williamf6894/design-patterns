public interface Visitable {

    // Allows the visitor to pass the object so
    // the right operations occur on the right type
    // of object

    // accept() is passed the visitor object but there the
    // method visit() is called using the visitor object. Due to
    // method overloading the right visit is called;

    public double accept(Visitor visitor);
}
