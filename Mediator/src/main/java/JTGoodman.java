public class JTGoodman extends Colleague{

    public JTGoodman(Mediator mediator) {
        super(mediator);

        System.out.println("JT Goodman signed up with the stock exchange\n");
    }
}
