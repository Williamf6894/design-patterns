public class StockOffer {

    private int stockShares = 0;
    private String stockSymbol = "";
    private int colleagueCode = 0;

    public StockOffer(int numberOfShares, String stock, int colleagueCode) {
        this.stockShares = numberOfShares;
        this.stockSymbol = stock;
        this.colleagueCode = colleagueCode;

    }

    public int getStockShares() {
        return stockShares;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public int getColleagueCode() {
        return colleagueCode;
    }

}
