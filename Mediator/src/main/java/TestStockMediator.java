public class TestStockMediator {

    /*
    Allows loose coupling by encapsulating the way disparate
    sets of objects interact and communicate with each other.
    Allows for the actions of each object set to vary
    independently of one another.
     */
    public static void main(String[] args) {
        StockMediator nyse = new StockMediator();

        GormanSlacks broker1 = new GormanSlacks(nyse);
        JTGoodman broker2 = new JTGoodman(nyse);

        broker1.saleOffer("MSFT", 100);
        broker1.saleOffer("GOOG", 50);

        broker2.buyOffer("MSFT", 100);
        broker2.saleOffer("NRG", 10);

        broker1.buyOffer("NRG", 10);

        nyse.getStockOfferings();
    }
}
