// This acts as an interfact for every Song (Leaf)
// and SongGroup (Composite) we create

public abstract class SongComponent {

    // We throw UnsupportedOperationException so that if
    // it doesn't make sense for a sing or song group to
    // inherit a method, they can just inherit the default implementation

    public void add(SongComponent songComponent) {
        throw new UnsupportedOperationException();
    }

    public void remove(SongComponent songComponent) {
        throw new UnsupportedOperationException();
    }

    public SongComponent getComponent(int componentIndex) {
        throw new UnsupportedOperationException();
    }

    public String getSongName() {
        throw new UnsupportedOperationException();
    }

    public String getBandName() {
        throw new UnsupportedOperationException();
    }

    public int getReleaseYear() {
        throw new UnsupportedOperationException();
    }

    public void displaySongInfo() {
        throw new UnsupportedOperationException();
    }
}
