public class Animal {

    private String name;
    private double height;
    private int weight;
    private String favFood;
    private double speed;
    private String sound;

    public Flyable flyingType;

    public String tryToFly() {
        return flyingType.fly();
    }

    public void setFlyingAbility(Flyable newFlyType) {
        flyingType = newFlyType;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSound() {
        return sound;
    }
    public void setSound(String sound) {
        this.sound = sound;
    }

    public double getSpeed() {
        return speed;
    }
    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String getFavFood() {
        return favFood;
    }
    public void setFavFood(String favFood) {
        this.favFood = favFood;
    }

    public int getWeight() {
        return weight;
    }
    public void setWeight(int weight) {
        if(isMoreThanZero(weight)) {
            this.weight = weight;
        }
    }

    public double getHeight() {
        return height;
    }
    public void setHeight(double height) {
        if(isMoreThanZero((int) height)) {
            this.height = height;
        }
    }

    private boolean isMoreThanZero(int input) {
        if (input > 0) {
            return true;
        }
        else {
            System.out.println("Error: Cannot set value to Zero or less");
            return false;
        }
    }

}
