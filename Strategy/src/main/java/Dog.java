public class Dog extends Animal{

    public Dog() {
       super();
       setSound("Bark");

       flyingType = new CannotFly();
    }

    public void dig() {
        System.out.println("Digging a hole");
    }
}
