public class AnimalMain {

    public static void main(String[]  args) {

        Animal dog = new Dog();
        Animal birdy = new Bird();

        System.out.println("Dog: " + dog.tryToFly());
        System.out.println("Bird: " + birdy.tryToFly());


        dog.setFlyingAbility(new CanFly());

        System.out.println("Dog: " + dog.tryToFly());


    }
}
