public class CannotFly implements Flyable {
    public String fly() {
        return "I cannot fly";
    }
}
